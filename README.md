## Service API Key
- 5 service API keys are defined in the application.yaml (weatherReport.apiKeys)
- Basically there are 2 types of service api keys - BASIC and PRO
- Basic api key is restricted to only saved report from database
- Pro api key will always fetch report from the OpenWeatherMap api, and the report will be saved into database

## Rate Limiting
- Using third party library bucket4j
- Configuration of rate limit is defined in the application.yaml (bucket4j)
  
## Weather Report API Specification
-------------------------------------------------------
- Description: Get weather report for a city/country
- Method: GET
- URL: http://localhost:8080/weather-report/api/weather/{city}/{country}
- HEADER: x-api-key
-------------------------------------------------------
Refer to the postman collection in the root folder/postman

## Local Environment
- Navigate to the root directory
```shell
cd weather-report
```

- Run the application in development mode using the Spring Boot Plugin run goal
```shell
mvn clean spring-boot:run
```
- Open [http://localhost:8080/weather-report/docs/swagger-ui.html](http://localhost:8080/weather-report/docs/swagger-ui.html) for API documentation
- Postman collection is in root directory/postman
- Using test data from resources/data.sql
