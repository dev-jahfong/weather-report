package au.jahfong.weatherreport.service;

import static au.jahfong.weatherreport.model.ServiceApiKeyType.BASIC;
import static au.jahfong.weatherreport.model.ServiceApiKeyType.PRO;

import au.jahfong.weatherreport.exception.InvalidServiceApiKey;
import au.jahfong.weatherreport.model.ServiceApiKey;
import au.jahfong.weatherreport.model.ServiceApiKeyType;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Implementation of the service api key validator.
 */
@Service
@Slf4j
public class ServiceApiKeyValidatorImpl implements ServiceApiKeyValidator {
    private final List<String> serviceApiKeys;

    @Autowired
    public ServiceApiKeyValidatorImpl(@Value("${weatherReport.apiKeys}") String[] apiKeys) {
        serviceApiKeys = Arrays.asList(apiKeys);
    }

    @Override
    public ServiceApiKey validate(String apiKey) {
        Validate.notNull(apiKey);

        if (!serviceApiKeys.contains(apiKey.toUpperCase())) {
            log.debug("Invalid service api key: {}", apiKey);
            throw new InvalidServiceApiKey();
        }

        ServiceApiKeyType apiKeyType = apiKey.startsWith("BASIC-") ? BASIC : PRO;
        return ServiceApiKey.builder().apiKey(apiKey).apiKeyType(apiKeyType).build();
    }
}
