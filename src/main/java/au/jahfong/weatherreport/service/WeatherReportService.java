package au.jahfong.weatherreport.service;

import au.jahfong.weatherreport.model.ServiceApiKey;
import au.jahfong.weatherreport.model.WeatherReport;

/**
 * WeatherReportService.
 */
public interface WeatherReportService {

    WeatherReport getWeatherReport(String city, String country, ServiceApiKey serviceApiKey);
}
