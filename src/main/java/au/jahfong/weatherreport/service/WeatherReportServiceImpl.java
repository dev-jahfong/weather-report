package au.jahfong.weatherreport.service;

import au.jahfong.weatherreport.exception.InvalidLocation;
import au.jahfong.weatherreport.exception.WeatherReportNotAvailable;
import au.jahfong.weatherreport.model.ServiceApiKey;
import au.jahfong.weatherreport.model.ServiceApiKeyType;
import au.jahfong.weatherreport.model.WeatherReport;
import au.jahfong.weatherreport.repository.WeatherReportRepository;
import com.fasterxml.jackson.databind.JsonNode;
import java.time.LocalDateTime;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

/**
 * Service to retrieve weather report.
 */
@Service
@Slf4j
public class WeatherReportServiceImpl implements WeatherReportService {
    private final WebClient webClient;
    private final String openWeatherMapApiKey;
    private final WeatherReportRepository weatherReportRepository;

    /**
     * Initializing the report service.
     *
     * @param webClient the OpenWeatherMap webclient
     *                  {@link au.jahfong.weatherreport.config.WebClientConfig}
     * @param openWeatherMapApiKey the OpenWeatherMap api key
     * @param weatherReportRepository the JPA repository for weather report
     */
    @Autowired
    public WeatherReportServiceImpl(
            @Qualifier("OpenWeatherMapWebClient") final WebClient webClient,
            @Value("${weatherReport.openweathermap.apiKey}") String openWeatherMapApiKey,
            final WeatherReportRepository weatherReportRepository) {
        this.webClient = webClient;
        this.openWeatherMapApiKey = openWeatherMapApiKey;
        this.weatherReportRepository = weatherReportRepository;
    }

    @Override
    public WeatherReport getWeatherReport(String city, String country, ServiceApiKey apKey) {
        if (apKey.getApiKeyType().equals(ServiceApiKeyType.PRO)) {
            return getWeatherReportFromOpenWeatherApi(city, country);
        } else {
            return getWeatherReportFromDb(city, country);
        }
    }

    private WeatherReport getWeatherReportFromDb(String city, String country) {
        log.info("Get weather report from DB");

        return weatherReportRepository.findByCityAndCountry(
                city.toLowerCase(),
                country.toLowerCase())
            .orElseThrow(WeatherReportNotAvailable::new);
    }

    private WeatherReport getWeatherReportFromOpenWeatherApi(String city, String country) {
        log.info("Get weather report from OpenWeatherAPI");

        return webClient.get().uri(uriBuilder -> uriBuilder
            .queryParam("q", city + "," + country)
            .queryParam("APPID", openWeatherMapApiKey)
            .build())
            .retrieve()
            .bodyToMono(JsonNode.class)
            .onErrorResume(WebClientResponseException.class, this::handleError)
            .map(r -> buildWeatherReport(city, country, r))
            .map(r -> saveWeatherReport(city, country, r))
            .block();
    }

    private WeatherReport saveWeatherReport(String city, String country,
                                            WeatherReport freshReport) {

        Optional<WeatherReport> weatherReport = weatherReportRepository.findByCityAndCountry(
                city.toLowerCase(), country.toLowerCase());

        if (weatherReport.isPresent()) {
            weatherReport.get().setWeather(freshReport.getWeather());
            weatherReport.get().setUpdatedOn(LocalDateTime.now());
            return weatherReportRepository.save(weatherReport.get());
        } else {
            return weatherReportRepository.save(freshReport);
        }
    }


    private WeatherReport buildWeatherReport(String city, String country, JsonNode weatherNode) {
        WeatherReport weatherReport = new WeatherReport();

        weatherReport.setWeather(weatherNode.path("weather").get(0).get("description").asText());
        weatherReport.setCity(city.toLowerCase());
        weatherReport.setCountry(country.toLowerCase());

        LocalDateTime now = LocalDateTime.now();
        weatherReport.setCreatedOn(now);
        weatherReport.setUpdatedOn(now);

        return weatherReport;
    }

    private Mono<JsonNode> handleError(WebClientResponseException ex) {
        return ex.getRawStatusCode() == 404 ? Mono.error(new InvalidLocation()) : Mono.error(ex);
    }
}
