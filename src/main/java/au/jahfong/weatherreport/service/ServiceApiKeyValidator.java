package au.jahfong.weatherreport.service;

import au.jahfong.weatherreport.model.ServiceApiKey;

/**
 * Service to validate the service API key.
 */
public interface ServiceApiKeyValidator {
    ServiceApiKey validate(String apiKey);
}
