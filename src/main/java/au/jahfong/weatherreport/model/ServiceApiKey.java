package au.jahfong.weatherreport.model;

import lombok.Builder;
import lombok.Data;

/**
 * Defines the Service API Key model.
 */
@Data
@Builder
public class ServiceApiKey {
    private final String apiKey;
    private final ServiceApiKeyType apiKeyType;
}
