package au.jahfong.weatherreport.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Defines the Weather Report model.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "WEATHER_REPORT")
public class WeatherReport {
    @JsonIgnore
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @NotBlank
    @Column(name = "CITY")
    private String city;

    @NotNull
    @NotBlank
    @Column(name = "COUNTRY")
    private String country;

    @NotNull
    @NotBlank
    @Column(name = "WEATHER")
    private String weather;

    @NotNull
    @Column(name = "CREATED_ON")
    private LocalDateTime createdOn;

    @NotNull
    @Column(name = "UPDATED_ON")
    private LocalDateTime updatedOn;
}
