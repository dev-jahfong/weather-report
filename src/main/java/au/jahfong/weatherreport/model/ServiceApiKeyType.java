package au.jahfong.weatherreport.model;

/**
 * Defines the API Key type.
 */
public enum ServiceApiKeyType {
    BASIC, PRO
}
