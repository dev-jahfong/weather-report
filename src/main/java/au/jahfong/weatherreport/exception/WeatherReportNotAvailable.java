package au.jahfong.weatherreport.exception;

/**
 * Exception for phone number already activated.
 */
public class WeatherReportNotAvailable extends RuntimeException {

}
