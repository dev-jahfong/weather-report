package au.jahfong.weatherreport.exception;

import static au.jahfong.weatherreport.exception.ErrorCode.INVALID_LOCATION;
import static au.jahfong.weatherreport.exception.ErrorCode.INVALID_SERVICE_API_KEY;
import static au.jahfong.weatherreport.exception.ErrorCode.WEATHER_REPORT_NOT_FOUND;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Exception Handler to handle all the exceptions thrown from the API.
 */
@RestControllerAdvice
@Slf4j
class RestControllerExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Handles the {@link InvalidLocation}.
     *
     * @param ex the exception
     * @return an instance of {@link ErrorResponse} with {@code PHONE_NUMBER_NOT_FOUND}
     */
    @ExceptionHandler(InvalidLocation.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ErrorResponse handleInvalidLocation(InvalidLocation ex) {
        log.error("handleInvalidLocation", ex);

        return new ErrorResponse(INVALID_LOCATION.value(), INVALID_LOCATION.getReasonPhrase());
    }


    /**
     * Handles the {@link InvalidServiceApiKey}.
     *
     * @param ex the exception
     * @return an instance of {@link ErrorResponse} with {@code INVALID_SERVICE_API_KEY}
     */
    @ExceptionHandler(InvalidServiceApiKey.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected ErrorResponse handleInvalidServiceApiKey(InvalidServiceApiKey ex) {
        log.error("handleInvalidServiceApiKey", ex);

        return new ErrorResponse(INVALID_SERVICE_API_KEY.value(),
            INVALID_SERVICE_API_KEY.getReasonPhrase());
    }

    /**
     * Handles the {@link WeatherReportNotAvailable}.
     *
     * @param ex the exception
     * @return an instance of {@link ErrorResponse} with {@code WEATHER_REPORT_NOT_FOUND}
     */
    @ExceptionHandler(WeatherReportNotAvailable.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected ErrorResponse handleWeatherReportNotAvailable(WeatherReportNotAvailable ex) {
        log.error("handleWeatherReportNotAvailable", ex);

        return new ErrorResponse(WEATHER_REPORT_NOT_FOUND.value(),
            WEATHER_REPORT_NOT_FOUND.getReasonPhrase());
    }
}
