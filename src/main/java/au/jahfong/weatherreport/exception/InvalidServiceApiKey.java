package au.jahfong.weatherreport.exception;

/**
 * Exception for invalid service API key.
 */
public class InvalidServiceApiKey extends RuntimeException {

}
