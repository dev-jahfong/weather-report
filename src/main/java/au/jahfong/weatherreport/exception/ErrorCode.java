package au.jahfong.weatherreport.exception;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Defines the list of error codes.
 */
public enum ErrorCode {

    INVALID_LOCATION("400", "The specified city/country does not exist."),
    INVALID_SERVICE_API_KEY("401", "The service api key is missing or invalid."),
    WEATHER_REPORT_NOT_FOUND("404",
        "This Weather report is not available from our DB, please upgrade to PRO.");

    private final String value;
    private final String reasonPhrase;

    ErrorCode(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    public String value() {
        return this.value;
    }

    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @JsonValue
    @Override
    public String toString() {
        return this.value + " " + this.reasonPhrase;
    }

}
