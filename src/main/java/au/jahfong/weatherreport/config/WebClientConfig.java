package au.jahfong.weatherreport.config;

import io.netty.handler.logging.LogLevel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.logging.AdvancedByteBufFormat;


/**
 * Defines configuration for WebClient.
 */
@Configuration
public class WebClientConfig {

    /**
     * WebClient Configuration for OpenWeatherMap.
     *
     * @param baseUrl the base url of the OpenWeatherMap weather api
     *
     * @return WebClient
     */
    @Bean
    @Qualifier(value = "OpenWeatherMapWebClient")
    public WebClient webClient(
        @Value("${weatherReport.openweathermap.weatherBaseUrl}") String baseUrl) {
        HttpClient httpClient = HttpClient
            .create()
            .wiretap("reactor.netty.http.client.HttpClient",
                LogLevel.DEBUG, AdvancedByteBufFormat.TEXTUAL);

        return WebClient
            .builder()
            .baseUrl(baseUrl)
            .clientConnector(new ReactorClientHttpConnector(httpClient))
            .build();
    }
}
