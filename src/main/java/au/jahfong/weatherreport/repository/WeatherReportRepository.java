package au.jahfong.weatherreport.repository;

import au.jahfong.weatherreport.model.WeatherReport;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The JPA Weather Report repository.
 */
public interface WeatherReportRepository  extends JpaRepository<WeatherReport, Long> {
    Optional<WeatherReport> findByCityAndCountry(String city, String country);
}
