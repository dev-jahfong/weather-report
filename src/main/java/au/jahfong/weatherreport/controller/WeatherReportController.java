package au.jahfong.weatherreport.controller;

import au.jahfong.weatherreport.model.ServiceApiKey;
import au.jahfong.weatherreport.model.WeatherReport;
import au.jahfong.weatherreport.service.ServiceApiKeyValidator;
import au.jahfong.weatherreport.service.WeatherReportService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Defines the Weather Report REST API.
 */
@RestController
@RequestMapping("/api")
@Tag(name = "Weather Report API")
@Validated
@Slf4j
public class WeatherReportController {

    private final WeatherReportService weatherReportService;
    private final ServiceApiKeyValidator serviceApiKeyValidator;

    @Autowired
    public WeatherReportController(final WeatherReportService weatherReportService,
                                   final ServiceApiKeyValidator serviceApiKeyValidator) {
        this.weatherReportService = weatherReportService;
        this.serviceApiKeyValidator = serviceApiKeyValidator;
    }

    /**
     * Get weather report for a particular city and country.
     *
     * @param city    the city to get report
     * @param country the country to get report
     * @return the WeatherReport
     */
    @Operation(summary = "Get weather report", responses = {
        @ApiResponse(responseCode = "200", description = "Successful"),
        @ApiResponse(responseCode = "400", description = "Bad Request"),
        @ApiResponse(responseCode = "401", description = "Unauthorized")
    })
    @GetMapping("/weather/{city}/{country}")
    public ResponseEntity<WeatherReport> getWeatherReport(
            @PathVariable String city,
            @PathVariable String country,
            @RequestHeader(value = "X-api-key") String apiKey) {

        ServiceApiKey serviceApiKey = serviceApiKeyValidator.validate(apiKey);

        WeatherReport weatherReport = weatherReportService.getWeatherReport(
            city, country, serviceApiKey);

        return ResponseEntity.ok(weatherReport);
    }
}
