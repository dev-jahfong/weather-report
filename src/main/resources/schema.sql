DROP TABLE IF EXISTS WEATHER_REPORT;

CREATE TABLE WEATHER_REPORT (
  ID INT AUTO_INCREMENT  PRIMARY KEY,
  CITY VARCHAR(80) NOT NULL,
  COUNTRY VARCHAR(80) NOT NULL,
  WEATHER VARCHAR(100) NOT NULL,
  CREATED_ON TIMESTAMP NOT NULL,
  UPDATED_ON TIMESTAMP NOT NULL
);
