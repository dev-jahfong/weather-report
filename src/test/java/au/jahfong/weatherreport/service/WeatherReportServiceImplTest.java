package au.jahfong.weatherreport.service;

import static org.junit.jupiter.api.Assertions.*;

import au.jahfong.weatherreport.exception.InvalidLocation;
import au.jahfong.weatherreport.exception.WeatherReportNotAvailable;
import au.jahfong.weatherreport.model.ServiceApiKey;
import au.jahfong.weatherreport.model.ServiceApiKeyType;
import au.jahfong.weatherreport.model.WeatherReport;
import au.jahfong.weatherreport.repository.WeatherReportRepository;
import java.io.IOException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@DirtiesContext
class WeatherReportServiceImplTest {
    static MockWebServer mockBackEnd;

    static final String LONDON_UK_WEATHER_REPORT_JSON = "{\"coord\":{\"lon\":-0.1257," +
        "\"lat\":51.5085},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":" +
        "\"overcast clouds\",\"icon\":\"04n\"}],\"base\":\"stations\",\"main\":{\"temp\":291.13," +
        "\"feels_like\":291.18,\"temp_min\":288.91,\"temp_max\":292.31,\"pressure\":1010," +
        "\"humidity\":84},\"visibility\":10000,\"wind\":{\"speed\":0.45,\"deg\":250," +
        "\"gust\":2.68},\"clouds\":{\"all\":100},\"dt\":1627348948,\"sys\":{\"type\":2," +
        "\"id\":2019646,\"country\":\"GB\",\"sunrise\":1627359414,\"sunset\":1627415813}," +
        "\"timezone\":3600,\"id\":2643743,\"name\":\"London\",\"cod\":200}\n";

    static final String MELBOURNE_AU_WEATHER_REPORT_JSON = "{\"coord\":{\"lon\":144.9633," +
        "\"lat\":-37.814},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":" +
        "\"light rain\",\"icon\":\"10d\"}],\"base\":\"stations\",\"main\":{\"temp\":288.33," +
        "\"feels_like\":287.47,\"temp_min\":286.31,\"temp_max\":291.05,\"pressure\":1005," +
        "\"humidity\":60},\"visibility\":10000,\"wind\":{\"speed\":4.02,\"deg\":347," +
        "\"gust\":8.05},\"rain\":{\"1h\":0.94},\"clouds\":{\"all\":49},\"dt\":1627349524,\"sys\":{" +
        "\"type\":2,\"id\":2008797,\"country\":\"AU\",\"sunrise\":1627334690,\"sunset\":1627370893},\"" +
        "timezone\":36000,\"id\":2158177,\"name\":\"Melbourne\",\"cod\":200}\n";


    @Autowired
    private WeatherReportRepository weatherReportRepository;

    private WeatherReportService weatherReportService;

    ServiceApiKey proApiKey = ServiceApiKey.builder().apiKey("PRO-001").apiKeyType(
        ServiceApiKeyType.PRO).build();

    ServiceApiKey basicApiKey = ServiceApiKey.builder().apiKey("BASIC-001").apiKeyType(
        ServiceApiKeyType.BASIC).build();

    @BeforeAll
    static void setUp() throws IOException {
        mockBackEnd = new MockWebServer();
        mockBackEnd.start();
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockBackEnd.shutdown();
    }

    @BeforeEach
    void setup() {
        weatherReportService = new WeatherReportServiceImpl(
            WebClient.create(String.format("http://localhost:%s", mockBackEnd.getPort())),
            "KEY-XXXX", weatherReportRepository);

    }

    @Test
    void should_getWeatherReport_from_DB_for_apiKey_type_BASIC() {
        // When
        WeatherReport weatherReport = weatherReportService.getWeatherReport(
            "sydney", "au", basicApiKey);

        // Then
        assertEquals("Sunny", weatherReport.getWeather());
        assertEquals("sydney", weatherReport.getCity());
        assertEquals("au", weatherReport.getCountry());
        assertNotNull(weatherReport.getCreatedOn());
        assertNotNull(weatherReport.getUpdatedOn());
    }

    @Test
    void should_getWeatherReport_from_OpenWeatherMap_for_apiKey_type_PRO() {
        // Given
        mockBackEnd.enqueue(new MockResponse()
            .setBody(LONDON_UK_WEATHER_REPORT_JSON)
            .addHeader("Content-Type", "application/json"));

        // When
        WeatherReport weatherReport = weatherReportService.getWeatherReport(
            "london", "uk", proApiKey);

        // Then
        assertEquals("overcast clouds", weatherReport.getWeather());
        assertEquals("london", weatherReport.getCity());
        assertEquals("uk", weatherReport.getCountry());
        assertNotNull(weatherReport.getCreatedOn());
        assertNotNull(weatherReport.getUpdatedOn());

        // -- check report is saved in DB
        WeatherReport saved = weatherReportRepository.findByCityAndCountry(
            "london", "uk").get();
        assertEquals(weatherReport.getWeather(), saved.getWeather());
        assertEquals(weatherReport.getCity(), saved.getCity());
        assertEquals(weatherReport.getCountry(), saved.getCountry());
        assertEquals(weatherReport.getCreatedOn(), saved.getCreatedOn());
        assertEquals(weatherReport.getUpdatedOn(), saved.getUpdatedOn());
    }

    @Test
    void should_updateWeatherReport_from_OpenWeatherMap_for_apiKey_type_PRO() {
        // Given
        mockBackEnd.enqueue(new MockResponse()
            .setBody(MELBOURNE_AU_WEATHER_REPORT_JSON)
            .addHeader("Content-Type", "application/json"));

        // When
        WeatherReport weatherReport = weatherReportService.getWeatherReport(
            "melbourne", "au", proApiKey);

        // Then
        assertEquals("light rain", weatherReport.getWeather());
        assertEquals("melbourne", weatherReport.getCity());
        assertEquals("au", weatherReport.getCountry());
        assertNotNull(weatherReport.getCreatedOn());
        assertNotNull(weatherReport.getUpdatedOn());

        // -- check report is saved in DB
        WeatherReport saved = weatherReportRepository.findByCityAndCountry(
            "melbourne", "au").get();
        assertEquals(weatherReport.getWeather(), saved.getWeather());
        assertEquals(weatherReport.getCity(), saved.getCity());
        assertEquals(weatherReport.getCountry(), saved.getCountry());
        assertEquals(weatherReport.getCreatedOn(), saved.getCreatedOn());
        assertEquals(weatherReport.getUpdatedOn(), saved.getUpdatedOn());
    }


    @Test
    void should_throw_invalidLocation_for_OpenWeatherMap_error_Not_Found() {
        // Given
        mockBackEnd.enqueue(new MockResponse()
            .setResponseCode(404)
            .addHeader("Content-Type", "application/json"));

        // When
        assertThrows(InvalidLocation.class, () ->weatherReportService.getWeatherReport(
            "latan", "virtual", proApiKey));
    }

    @Test
    void should_rethrow_WebClientResponseException_for_OpenWeatherMap_WebClientResponseException() {
        // Given
        mockBackEnd.enqueue(new MockResponse()
            .setResponseCode(500)
            .addHeader("Content-Type", "application/json"));

        // When
        assertThrows(WebClientResponseException.class, () ->weatherReportService.getWeatherReport(
            "latan", "virtual", proApiKey));
    }

    @Test
    void should_throw_WeatherReportNotAvailable_for_report_not_available_in_DB_for_BASIC_api_type() {
        // When
        assertThrows(WeatherReportNotAvailable.class, () ->weatherReportService.getWeatherReport(
            "Perth", "AU", basicApiKey));
    }

}