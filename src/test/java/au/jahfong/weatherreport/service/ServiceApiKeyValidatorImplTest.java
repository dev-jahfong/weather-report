package au.jahfong.weatherreport.service;

import static org.junit.jupiter.api.Assertions.*;

import au.jahfong.weatherreport.exception.InvalidServiceApiKey;
import au.jahfong.weatherreport.model.ServiceApiKey;
import au.jahfong.weatherreport.model.ServiceApiKeyType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ServiceApiKeyValidatorImplTest {
    private ServiceApiKeyValidator serviceApiKeyValidator;

    private String[] apiKeys = { "BASIC-001", "BASIC-002", "PRO-001" };

    @BeforeEach
    void setup() {
        serviceApiKeyValidator = new ServiceApiKeyValidatorImpl(apiKeys);
    }

    @Test
    void should_validate_service_api_key_successfully() {
        ServiceApiKey basicKey001 = serviceApiKeyValidator.validate("BASIC-001");

        assertServiceApiKey("BASIC-001", ServiceApiKeyType.BASIC);
        assertServiceApiKey("BASIC-002", ServiceApiKeyType.BASIC);
        assertServiceApiKey("PRO-001", ServiceApiKeyType.PRO);
    }

    @Test
    void should_throw_InvalidServiceApiKey_for_invalid_api_key() {
        assertThrows(InvalidServiceApiKey.class, () ->
            serviceApiKeyValidator.validate("BASIC-XXX"));
    }

    void assertServiceApiKey(String apiKey, ServiceApiKeyType expectedApiType) {
        ServiceApiKey serviceApiKey = serviceApiKeyValidator.validate(apiKey);

        assertEquals(apiKey, serviceApiKey.getApiKey());
        assertEquals(expectedApiType, serviceApiKey.getApiKeyType());
    }
}