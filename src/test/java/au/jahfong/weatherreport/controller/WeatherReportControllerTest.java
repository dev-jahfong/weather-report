package au.jahfong.weatherreport.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import au.jahfong.weatherreport.exception.InvalidLocation;
import au.jahfong.weatherreport.exception.InvalidServiceApiKey;
import au.jahfong.weatherreport.exception.WeatherReportNotAvailable;
import au.jahfong.weatherreport.model.ServiceApiKey;
import au.jahfong.weatherreport.model.ServiceApiKeyType;
import au.jahfong.weatherreport.model.WeatherReport;
import au.jahfong.weatherreport.service.ServiceApiKeyValidator;
import au.jahfong.weatherreport.service.WeatherReportService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(WeatherReportController.class)
@ExtendWith(MockitoExtension.class)
class WeatherReportControllerTest {

    @MockBean
    private WeatherReportService weatherReportService;

    @MockBean
    private ServiceApiKeyValidator serviceApiKeyValidator;

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = createObjectMapper();

    private LocalDateTime now = LocalDateTime.now();

    private ServiceApiKey apiKey = ServiceApiKey.builder().apiKey("BASIC-001").apiKeyType(
        ServiceApiKeyType.BASIC).build();

    @BeforeEach
    void setup() {
        when(serviceApiKeyValidator.validate(apiKey.getApiKey())).thenReturn(apiKey);
    }

    @Test
    void should_get_weather_report() throws Exception {
        WeatherReport weatherReport = new WeatherReport();
        weatherReport.setCity("melbourne");
        weatherReport.setCountry("au");
        weatherReport.setWeather("Clear Sky");
        weatherReport.setCreatedOn(now);
        weatherReport.setUpdatedOn(now);

        when(weatherReportService.getWeatherReport("melbourne", "au", apiKey))
            .thenReturn(weatherReport);

        // When
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
            .get("/api/weather/melbourne/au")
            .header("X-api-key", apiKey.getApiKey())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();

        // Then
        JSONAssert.assertEquals(objectMapper.writeValueAsString(weatherReport),
            result.getResponse().getContentAsString(), false);
    }

    @Test
    void should_return_400_for_INVALID_LOCATION() throws Exception {
        // Given
        doThrow(new InvalidLocation()).when(weatherReportService).getWeatherReport(
            "sydney", "au", apiKey);

        // When
        mockMvc.perform(MockMvcRequestBuilders
            .get("/api/weather/sydney/au")
            .header("X-api-key", apiKey.getApiKey())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.errorCode", is("400")))
            .andExpect(jsonPath("$.errorMessage", is("The specified city/country does not exist.")));
    }

    @Test
    void should_return_401_for_INVALID_SERVICE_API_KEY() throws Exception {
        // Given
        String apiKey = "invalid-key";
        doThrow(new InvalidServiceApiKey()).when(serviceApiKeyValidator).validate(apiKey);

        // When
        mockMvc.perform(MockMvcRequestBuilders
            .get("/api/weather/sydney/au")
            .header("X-api-key", apiKey)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isUnauthorized())
            .andExpect(jsonPath("$.errorCode", is("401")))
            .andExpect(jsonPath("$.errorMessage", is("The service api key is missing or invalid.")));
    }

    @Test
    void should_return_404_for_Report_not_available_in_DB() throws Exception {
        // Given
        doThrow(new WeatherReportNotAvailable()).when(weatherReportService).getWeatherReport(
            "sydney", "au", apiKey);

        // When
        mockMvc.perform(MockMvcRequestBuilders
            .get("/api/weather/sydney/au")
            .header("X-api-key", apiKey.getApiKey())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.errorCode", is("404")))
            .andExpect(jsonPath("$.errorMessage", is("This Weather report is not available from our DB, please upgrade to PRO.")));
    }

    private ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

}