package au.jahfong.weatherreport.repository;

import static org.junit.jupiter.api.Assertions.*;

import au.jahfong.weatherreport.model.WeatherReport;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class WeatherReportRepositoryTest {

    @Autowired
    private WeatherReportRepository weatherReportRepository;

    @Test
    void should_find_By_City_Country() {
        Optional<WeatherReport>
            found = weatherReportRepository.findByCityAndCountry("melbourne", "au");

        assertTrue(found.isPresent());
        assertEquals("au", found.get().getCountry());
        assertEquals("melbourne", found.get().getCity());
        assertEquals("Clear Sky", found.get().getWeather());
        assertNotNull(found.get().getCreatedOn());
        assertNotNull(found.get().getUpdatedOn());
    }

}